defmodule MakinaExamplesTest do
  use PropCheck
  use ExUnit.Case, async: true

  import PropCheck.StateM

  describe "Counter" do
    @tag :counter
    property "correctness" do
      initial_value = 0
      model = Counter.Model.Behaviour

      forall cmds <- commands(model) do
        {:ok, _pid} = Counter.start_link(initial_value)
        r = {_, _, result} = run_commands(model, cmds)
        Agent.stop(Counter)

        (result == :ok)
        |> when_fail(print_report(r, cmds))
        |> aggregate(command_names(cmds))
      end
    end
  end

  describe "OtherCounter" do
    @tag :other_counter
    property "Example.OtherCounter.Model.Generators" do
      check_model(Example.OtherCounter.Model.Generators.Behaviour)
    end

    @tag :other_counter
    property "Example.OtherCounter.Model.Checks" do
      check_model(Example.OtherCounter.Model.Checks.Behaviour)
    end
  end

  describe "Clock" do
    def check_clock(model) do
      forall cmds <- commands(model) do
        trap_exit do
          r = run_commands(model, cmds)
          {_history, state, result} = r

          for {clock, _} <- state.clocks, do: Example.Clock.stop(clock)

          (result == :ok)
          |> when_fail(print_report(r, cmds))
          |> aggregate(command_names(cmds))
        end
      end
    end

    @tag :clock
    property "Example.Clock.Model.Generators" do
      check_clock(Example.Clock.Model.Generators.Behaviour)
    end

    @tag :clock
    property "Example.Clock.Model.Checks" do
      check_clock(Example.Clock.Model.Checks.Behaviour)
    end
  end

  describe "Store" do
    @tag :store
    property "Store.Model.CallbackStyle" do
      check_model(Example.Store.Model.CallbackStyle)
    end

    @tag :store
    property "Store.Model.Monolithic" do
      check_model(Example.Store.Model.Monolithic.Behaviour)
    end

    @tag :store
    property "Store.Model.Generators" do
      check_model(Example.Store.Model.Generators.Behaviour)
    end

    @tag :store
    property "Store.Model.Judge" do
      check_model(Example.Store.Model.Judge.Behaviour)
    end

    @tag :store
    property "Store.Model.Parallel.Judge" do
      check_model(Example.Store.Model.Parallel.Judge.Behaviour)
    end
  end

  describe "Secret" do
    @tag :secret
    property "Secret.Model" do
      check_model(Example.Secret.Model.Behaviour)
    end
  end

  describe "OTP" do
    def check_otp(model) do
      forall cmds <- commands(model) do
        r = {_, state, result} = run_commands(model, cmds)

        if state.otp_server != nil, do: Agent.stop(state.otp_server)

        (result == :ok)
        |> when_fail(print_report(r, cmds))
        |> aggregate(command_names(cmds))
      end
    end

    @tag :otp
    property "OTP.Model" do
      check_otp(Example.OTP.Model.Behaviour)
    end
  end

  describe "FizzBuzz" do
    def check_fizzbuzz(model) do
      forall cmds <- commands(model) do
        FizzBuzz.start(0)
        r = {_, _, result} = run_commands(model, cmds)
        Agent.stop(FizzBuzz)

        (result == :ok)
        |> when_fail(print_report(r, cmds))
        |> aggregate(command_names(cmds))
      end
    end

    @tag :fizzbuzz
    property "DigitFizzBuzzFizzbuzz" do
      check_fizzbuzz(DigitFizzBuzzFizzbuzz)
    end

    # @tag :fizzbuzz
    # property "Buzz" do
    #   check_fizzbuzz(Buzz)
    # end

    # @tag :fizzbuzz
    # property "Digit" do
    #   check_fizzbuzz(Digit)
    # end

    # @tag :fizzbuzz
    # property "FizzBuzz" do
    #   check_fizzbuzz(DigitFizzBuzz)
    # end
  end

  describe "ExCrypto" do
    @tag :ex_crypto
    property "ExCrypto" do
      check_model(ExCrypto.Model)
    end
  end

  ##############################################################################
  # helpers
  ##############################################################################

  def check_model(model) do
    forall cmds <- commands(model) do
      r = {_, _, result} = run_commands(model, cmds)

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
