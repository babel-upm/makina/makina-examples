import Config

config :logger, level: :error
config :makina, checker: PropCheck
