defmodule Example.Store.Model.NewGenerator do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command new() :: Store.store() do
    next stores: [result | stores]
  end
end

defmodule Example.Store.Model.GetGenerator do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command get(store :: Store.store()) :: integer do
    pre stores != []
    args store: oneof(stores)
  end
end

defmodule Example.Store.Model.PutGenerator do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command put(store :: Store.store(), value :: integer) :: integer do
    pre stores != []
    args store: oneof(stores), value: nat()
  end
end

defmodule Example.Store.Model.Parallel.Judge do
  use Makina,
    extends: [
      Example.Store.Model.NewGenerator,
      Example.Store.Model.PutGenerator,
      Example.Store.Model.GetGenerator
    ]

  state values: %{} :: %{symbolic(Store.store()) => integer()}

  command new() :: Store.store() do
    next super() ++ [values: Map.put(values, result, 0)]
  end

  command get(store :: Store.store()) :: integer do
    post Map.get(values, store) == result
  end

  command put(store :: Store.store(), value :: integer) :: integer do
    next super() ++ [values: Map.put(values, store, value)]
  end
end
