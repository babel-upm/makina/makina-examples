# can we say implemented_by here or somewhere else?
defmodule Example.Store.Model.Monolithic do
  use Makina, implemented_by: Store

  # state has an attribute stores : %{Store.store => integer}
  state stores: %{} :: %{symbolic(Store.store()) => integer()}

  command new() :: Store.store() do
    next stores: Map.put(stores, result, 0)
  end

  command put(store :: Store.store(), value :: integer) :: integer do
    pre map_size(stores) > 0
    args store: oneof(Map.keys(stores)), value: integer()
    next stores: Map.put(stores, store, value)
  end

  command get(store :: Store.store()) :: integer do
    pre map_size(stores) > 0
    args store: oneof(Map.keys(stores))
    post Map.get(stores, store) == result
  end
end
