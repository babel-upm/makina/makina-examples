defmodule Example.Store.Model.Generators do
  use Makina, implemented_by: Store

  # The state has an attribute which is a list (set) of stores
  state stores: [] :: [symbolic(Store.store())]

  command new() :: Store.store() do
    next stores: [result | stores]
  end

  command get(store :: Store.store()) :: integer do
    pre stores != []
    args store: oneof(stores)
  end

  command put(store :: Store.store(), value :: integer) :: integer do
    pre stores != []
    args store: oneof(stores), value: nat()
  end
end

defmodule Example.Store.Model.Judge do
  use Makina, extends: Example.Store.Model.Generators

  # The state has an attribute which is a map Store.store => integer()
  state values: %{} :: %{symbolic(Store.store()) => integer()}

  command new() do
    next super() ++ [values: Map.put(values, result, 0)]
  end

  command get() do
    post Map.get(values, store) == result
  end

  command put() do
    next super() ++ [values: Map.put(values, store, value)]
  end
end
