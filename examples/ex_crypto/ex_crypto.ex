defmodule ExCrypto.Model do
  import ExUnit.Assertions
  use Makina

  state keys: [], texts: [], cipher_texts: []

  invariants same_length: length(texts) == length(cipher_texts)

  command generate_key() do
    call ExCrypto.generate_aes_key(:aes_256, :bytes)
    next keys: [symbolic(Kernel.elem(result, 1)) | keys]
  end

  command encrypt(key, text) do
    pre keys != []
    args key: oneof(keys), text: binary()
    call ExCrypto.encrypt(key, text)
    next texts: [{key, text} | texts],
         cipher_texts: [{key, symbolic(Kernel.elem(result, 1))} | cipher_texts]
  end

  command decrypt(key, text) do
    pre texts != [] and cipher_texts != []
    args let {key, text} <- oneof(cipher_texts), do: [key: key, text: text]
    call ExCrypto.decrypt(key, elem(text, 0), elem(text, 1))
    post do
      assert {:ok, original_text} = result
      assert {key, original_text} in texts
    end
  end
end
