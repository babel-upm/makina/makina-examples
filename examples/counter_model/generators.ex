defmodule Example.OtherCounter.Model.Generators do
  use Makina, implemented_by: Example.OtherCounter

  state counters: [] :: [symbolic(pid)]

  invariants counters_list: is_list(counters)

  command new() :: pid do
    next counters: [result | state.counters]
  end

  command put(counter :: pid, value :: integer) :: integer do
    pre counters != []
    args value: nat(), counter: oneof(counters)
  end

  command get(counter :: pid) :: integer do
    pre counters != []
    args counter: oneof(counters)
  end
end
