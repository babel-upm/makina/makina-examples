defmodule Example.OtherCounter.Model.Checks do
  use Makina, extends: Example.OtherCounter.Model.Generators

  state values: %{} :: %{symbolic(pid) => integer()}

  invariants constant_size: length(Map.keys(values)) == length(counters)

  command new() do
    next super() ++ [values: Map.put(values, result, 0)]
  end

  command put() do
    next super() ++ [values: Map.put(values, counter, value)]
  end

  command get() do
    post result == values[counter]
  end
end
