defmodule Example.Clock.Model.Generators do
  use Makina, implemented_by: Example.Clock

  alias Example.Clock

  @type clock() :: pid()

  state clocks: %{} :: %{symbolic(clock()) => nil | integer()}

  command new() :: clock() do
    call Clock.start_link() |> elem(1)
    next clocks: Map.put(clocks, result, nil)
  end

  command tick(clock :: clock()) :: :ok do
    pre clocks != %{}
    args clock: oneof(Map.keys(clocks))
  end

  command time(clock :: clock()) :: integer() do
    pre clocks != %{}
    args clock: oneof(Map.keys(clocks))
  end
end
