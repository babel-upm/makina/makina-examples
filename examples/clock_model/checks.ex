defmodule Example.Clock.Model.Checks do
  use Makina, extends: Example.Clock.Model.Generators

  @type clock() :: pid()

  command tick() do
    next do
      case clocks[clock] do
        nil ->
          []

        time ->
          value = Kernel.+(1, time) |> symbolic()
          [clocks: Map.put(state.clocks, clock, value)]
      end
    end
  end

  command time() do
    post do
      case clocks[clock] do
        nil -> true
        _time -> result == 1 + rem(clocks[clock] - 1, 12)
      end
    end
  end
end
