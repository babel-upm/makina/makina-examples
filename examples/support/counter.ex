defmodule Counter do
  require Agent

  @spec start_link(integer()) :: {:error, integer()} | {:ok, pid()}
  def start_link(initial_value) do
    Agent.start_link(fn -> initial_value end, name: __MODULE__)
  end

  @spec stop() :: :ok
  def stop() do
    Agent.stop(__MODULE__)
  end

  @spec inc() :: :ok
  def inc() do
    Agent.update(__MODULE__, fn value -> value + 1 end)
  end

  @spec put(integer()) :: :ok
  def put(n) do
    Agent.update(__MODULE__, fn _value -> n end)
  end

  @spec get() :: integer()
  def get() do
    Agent.get(__MODULE__, & &1)
  end
end

defmodule Example.OtherCounter do
  def new do
    spawn(fn -> value(0) end)
  end

  def put(c, n) do
    send(c, {:set, n})
  end

  def get(c) do
    send(c, {:get, self()})

    receive do
      value -> value
    end
  end

  def value(n) do
    receive do
      {:set, m} ->
        value(m)

      {:get, pid} ->
        send(pid, n)
        value(n)
    end
  end
end
