# Makina examples

This repository contains some examples showing some functionalities of the DSL provided by [Makina](https://gitlab.com/babel-upm/makina/makina).


## Repository structure

Models are stored inside the directory `examples`. The properties to execute these models are under
the `test` directory. Tests can be executed using:

    $ mix test
