defmodule MakinaExamples.MixProject do
  use Mix.Project

  def project do
    [
      elixirc_paths: elixirc_paths(Mix.env()),
      app: :makina_examples,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_options: [{:tracers, [Makina.Tracer]}]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:makina, git: "git@gitlab.com:babel-upm/makina/makina.git", branch: "master"},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:propcheck, "~> 1.4", only: [:test, :dev]},
      {:ex_crypto, git: "https://github.com/ntrepid8/ex_crypto.git", branch: "master"}
      # {:ex_crypto, "~> 0.10.0"}
    ]
  end

  defp elixirc_paths(:test), do: elixirc_paths(:dev) ++ ["test"]

  defp elixirc_paths(_) do
    [
      "lib",
      "examples",
      "examples/basic_counter",
      "examples/clock_model",
      "examples/counter_model",
      "examples/fizz_buzz",
      "examples/otp",
      "examples/paper",
      "examples/secrets",
      "examples/store",
      "examples/support"
    ]
  end
end
